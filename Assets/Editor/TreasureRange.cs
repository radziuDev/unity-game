using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Treasure))]
public class TreasureRange : Editor {
  private void OnSceneGUI() {
    Treasure treasure = (Treasure)target;
    Handles.color = Color.red;
    Handles.DrawWireArc(treasure.transform.position, Vector3.up, Vector3.forward, 360, treasure.pickRadius);
  }
}
