using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Patrol))]
public class PatrolTrail : Editor {
  private void OnSceneGUI() {
    Patrol pathVisualizer = (Patrol)target;

    Vector3 startPoint = pathVisualizer.transform.position;
    Vector3 endPoint = pathVisualizer.transform.position + pathVisualizer.transform.forward * pathVisualizer.distance;

    Handles.color = Color.red;
    if (pathVisualizer.distance > 0) Handles.DrawLine(startPoint, endPoint);
  }
}
