using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(FOV))]
public class FieldOfView : Editor {
  private void OnSceneGUI() {
    FOV fov = (FOV)target;
    Handles.color = Color.white;
    Handles.DrawWireArc(fov.transform.position, Vector3.up, Vector3.forward, 360, fov.radius);
    Handles.color = Color.yellow;
    Handles.DrawWireArc(fov.transform.position, Vector3.up, Vector3.forward, 360, fov.alertRadius);

    Vector3 viewAngle01 = DirectionFromAngle(fov.transform.eulerAngles.y, -fov.angle / 2);
    Vector3 viewAngle02 = DirectionFromAngle(fov.transform.eulerAngles.y, fov.angle / 2);
    
    if (!fov.isEnemy) {
      viewAngle01 = DirectionFromAngle(Camera.main.transform.eulerAngles.y, -fov.angle / 2);
      viewAngle02 = DirectionFromAngle(Camera.main.transform.eulerAngles.y, fov.angle / 2);
    }

    Handles.color = Color.yellow;
    Handles.DrawLine(fov.transform.position, fov.transform.position + viewAngle01 * fov.radius);
    Handles.DrawLine(fov.transform.position, fov.transform.position + viewAngle02 * fov.radius);

    if (fov.canSeeTarget && fov.target) {
      Handles.color = Color.green;
      Handles.DrawLine(fov.transform.position, fov.target.transform.position);
    }
  }

  private Vector3 DirectionFromAngle(float eulerY, float angleInDegrees) {
    angleInDegrees += eulerY;
    return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
  }
}
