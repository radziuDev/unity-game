using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Alarm))]
public class AlarmArea : Editor {
  private void OnSceneGUI() {
    Alarm alarm = (Alarm)target;
    Handles.color = Color.white;
    Handles.DrawWireArc(alarm.transform.position, Vector3.up, Vector3.forward, 360, alarm.alertRadius);
    
    Handles.color = Color.red;
    Handles.DrawWireArc(alarm.transform.position, Vector3.up, Vector3.forward, 360, alarm.runRadius);
  }
}
