using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour {
  private Patrol patrolSctipt;
  private NavMeshAgent agent;
  private FOV FOVScript;
  
  private enum Warrior {
    Melee,
    Range,
  };

  [SerializeField]
  private Warrior warriorType;

  [SerializeField]
  private GameObject weapon;
  private Weapon weaponItem;
  [SerializeField]
  private GameObject ammunition;

  [SerializeField]
  private float movementSpeed = 6f;
  [SerializeField]
  private float rotationSpeed = 12f;
  [SerializeField]
  private AnimationCurve jumpCurve = new AnimationCurve();
  public float atackRange = 3f;
  
  private Animator animator;
  private StatusBar status;

  private bool isAbleToAtack = true;
  private bool isAbleToRotate = true;

  private void Start() {
    if (weapon) weapon.SetActive(true);
    if (ammunition) ammunition.SetActive(true);

    status = GetComponent<StatusBar>();
    patrolSctipt = GetComponent<Patrol>();
    agent = GetComponent<NavMeshAgent>();
    FOVScript = GetComponent<FOV>();
    animator = GetComponent<Animator>();

    weaponItem = weapon.GetComponent<Weapon>();
    weaponItem.enabled = true;

    StartCoroutine(HandleJumps());
  }

  private void Update() {
    animator.SetBool("isWalking", false);

    if (
      !FOVScript.canSeeTarget
      || !FOVScript.target
      || !isAbleToRotate
      || patrolSctipt.isPatroling
      || patrolSctipt.targetedAlarm
    ) return;
    
    Vector3 directionToTarget = (FOVScript.target.position - transform.position).normalized;
    Vector3 moveDirection = new Vector3(directionToTarget.x, 0f, directionToTarget.z);
    Quaternion lookRotation = Quaternion.LookRotation(moveDirection);
    float distanceToTarget = Vector3.Distance(
      new Vector3(FOVScript.target.position.x, 0, FOVScript.target.position.z),
      new Vector3(transform.position.x, 0, transform.position.z)
    );

    if (distanceToTarget < atackRange && isAbleToAtack && !weaponItem.isActive) {
      transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, rotationSpeed * Time.deltaTime);
      agent.enabled = false;

      HandleAtack(distanceToTarget, lookRotation);
      return;
    } 
    
    if (distanceToTarget < 2f) {      
      Vector3 newPosition = transform.position + moveDirection.normalized * movementSpeed * Time.deltaTime * -2;
      newPosition.y = transform.position.y;
      transform.position = newPosition;
      HandleMovement(newPosition, movementSpeed);  
    } else if (distanceToTarget < FOVScript.radius && distanceToTarget > atackRange) HandleMovement(FOVScript.target.position, movementSpeed);   
  }

  public void HandleMovement(
    Vector3 position,
    float movementSpeed = 10f
  ) {
    agent.enabled = true;
    agent.speed = movementSpeed;
    agent.SetDestination(position);
    animator.SetBool("isWalking", true);
  }

  IEnumerator HandleJumps() {
    NavMeshAgent agent = GetComponent<NavMeshAgent>();
    agent.autoTraverseOffMeshLink = false;
    while (true) {
      if (agent.isOnOffMeshLink) {
        yield return StartCoroutine(Curve(agent, 0.5f));
        agent.CompleteOffMeshLink();
      }
      yield return null;
    }
  }

  IEnumerator Curve(NavMeshAgent agent, float duration) {
    OffMeshLinkData data = agent.currentOffMeshLinkData;
    Vector3 startPos = agent.transform.position;
    Vector3 endPos = data.endPos + Vector3.up * agent.baseOffset;
    float normalizedTime = 0.0f;
    while (normalizedTime < 1.0f) {
      float yOffset = jumpCurve.Evaluate(normalizedTime);
      agent.transform.position = Vector3.Lerp(startPos, endPos, normalizedTime) + yOffset * Vector3.up;
      normalizedTime += Time.deltaTime / duration;
      yield return null;
    }
  }

  private void HandleAtack(float distanceToTarget, Quaternion lookRotation) {
    if (Quaternion.Angle(transform.rotation, lookRotation) > 5f) return;
    
    weaponItem.PerformAtack(1.2f);
    animator.SetTrigger( warriorType == Warrior.Range ? "rangeAtackEvent" : "atackEvent");

    isAbleToAtack = false;
    Invoke("ClearRotation", 0.5f);
    Invoke("ClearAtack", 1.5f);
  }

  private void ClearRotation() => isAbleToRotate = false;
  private void ClearAtack() {
    isAbleToAtack = true;
    isAbleToRotate = true;
  }
}
