using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Treasure : MonoBehaviour {
  [SerializeField]
  private LayerMask playerMask;
  public float pickRadius;

  [SerializeField]
  private Dock dock;
  private bool active = true;

  private void Start() {
    StartCoroutine(TreasureRoutine());
  }

  private IEnumerator TreasureRoutine() {
    WaitForSeconds wait = new WaitForSeconds(0.2f);
    
    while (true) {
      yield return wait;
      TreasureCheck();
    }
  }

  private void TreasureCheck () {
    Collider[] players = Physics.OverlapSphere(transform.position, pickRadius, playerMask);
    if (players.Length > 0 && active) {
      active = false;
      PickUp();
    }
  }

  private void PickUp() {
    dock.treasureCounter += 1;
    Destroy(gameObject);
  }
}
