using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class CharacterStats {
  public float maxHealth = 100;
  public float health = 100;
  public float stamina = 100;
  public float mana = 100;
}

public class StatusBar : MonoBehaviour {
  [SerializeField]
  private bool enemy = false;

  [SerializeField]
  private float health = 100;
  private float maxHealth = 100;
  [SerializeField]
  private float stamina = 100;
  private float maxStamina = 100;
  [SerializeField]
  private float mana = 100;
  private float maxMana = 100;

  public GameObject UIBar;
  public Slider healthSlider;
  public Slider staminaSlider;
  public Slider manaSlider;
  public GameObject target;

  private FOV fov;

  private void Start() {
    if (enemy) {
      healthSlider.gameObject.SetActive(false);
      staminaSlider.gameObject.SetActive(false);
      manaSlider.gameObject.SetActive(false);
      target.gameObject.SetActive(false);
    }

    fov = gameObject.GetComponent<FOV>();

    healthSlider.maxValue = health;
    staminaSlider.maxValue = stamina;
    manaSlider.maxValue = mana;

    healthSlider.minValue = 0;
    staminaSlider.minValue = 0;
    manaSlider.minValue = 0;

    healthSlider.value = health;
    staminaSlider.value = stamina;
    manaSlider.value = mana;

    maxHealth = health;
    maxStamina = stamina;
    maxMana = mana;

    StartCoroutine("RestoreResources");
  }

  private void Update() {
    healthSlider.value = health;
    staminaSlider.value = stamina;
    manaSlider.value = mana;
  }

  private IEnumerator RestoreResources() {
    float staminaRegen = 2;
    float manaRegen = 2;

    for (;;) {
      if (mana < maxMana) mana += manaRegen;
      else mana = maxMana;

      if (stamina < maxStamina) stamina += staminaRegen;
      else stamina = maxStamina;

      yield return new WaitForSeconds(0.5f);
    }
  }

  public void TakeDamage(float damage) {
    if (enemy) healthSlider.gameObject.SetActive(true);
    if (enemy && !fov.agressiveMode) {
      damage = damage * 3;
      fov.StartRageMode();
    }

    health -= damage;

    if (health <= 0f) {
      if (enemy) UIBar.SetActive(false);
      Destroy(gameObject, 0.5f);
    }
  }

  public void DrainStamina(float drainedStamina) {
    stamina -= drainedStamina;
  }
  public void DrainMana(float drainedMana) {
    mana -= drainedMana;
  }

  public void Heal(float hp) {
    health += hp;
  }

  public CharacterStats TakeSnapshot() => new CharacterStats {
    maxHealth = maxHealth,
    health = health,
    stamina = stamina,
    mana = mana
  };

  public void setTarget(bool isTargeted) {
    if (enemy) target.gameObject.SetActive(isTargeted);
  }
}
