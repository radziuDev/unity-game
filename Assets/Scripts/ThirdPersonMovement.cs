using System.Collections;
using UnityEngine;

public class ThirdPersonMovement : MonoBehaviour {
  private CharacterController controller;
  private CameraLock cameraLock;
  private Animator animator;
  private StatusBar status;
  
  [SerializeField]
  private float speed = 6;
  private float turnSmoothVelocity;
  private float turnSmoothTime = 0.1f;
  [SerializeField]
  private float gravity = -9.81f;
  [SerializeField]
  private float jumpHeight = 3;
  private Vector3 velocity;
  
  [SerializeField]
  private Transform groundCheck;
  [SerializeField]
  private LayerMask groundMask;
  [SerializeField]
  private float groundDistance = 0.4f;
  [HideInInspector]
  public bool isGrounded;

  [SerializeField]
  private GameObject weapon;
  [HideInInspector]
  public Weapon weaponItem;

  [HideInInspector]
  public bool isDashing = false;
  private bool isSprinting = false;

  private void Start() {
    weaponItem = weapon.gameObject.GetComponent<Weapon>();
    controller = gameObject.GetComponent<CharacterController>();
    cameraLock = gameObject.gameObject.GetComponent<CameraLock>();
    status = gameObject.GetComponent<StatusBar>();
    animator = gameObject.GetComponent<Animator>();

    StartCoroutine(SprintChecker());
  }

  private void Update() {
    HandleAtack();

    float horizontal = Input.GetAxisRaw("Horizontal");
    float vertical = Input.GetAxisRaw("Vertical");
    float acctualSpeed = speed;

    animator.SetBool("isMovesRight", false);
    animator.SetBool("isMovesLeft", false);
    animator.SetBool("isSprinting", false);
    animator.SetBool("isLocked", false);
    isSprinting = false;

    isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
    if (isGrounded && velocity.y < 0) {
      animator.SetBool("isJumping", false);
      velocity.y = -2f;
    }

    if (Input.GetButtonDown("Jump") && isGrounded) {
      animator.SetBool("isJumping", true);
      velocity.y = Mathf.Sqrt(jumpHeight * -2 * gravity);
    }
    
    velocity.y += gravity * Time.deltaTime;
    controller.Move(velocity * Time.deltaTime);

    Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;
    if (direction.magnitude < 0.1f) {
      animator.SetBool("isWalking", false);
      animator.SetBool("isSprinting", false);
    }

    if (cameraLock.locked) animator.SetBool("isLocked", true);
    if (horizontal > 0) animator.SetBool("isMovesRight", true);
    if (horizontal < 0) animator.SetBool("isMovesLeft", true);

    CharacterStats acctualState = status.TakeSnapshot();
    if (
      Input.GetKey(KeyCode.LeftControl)
      && acctualState.stamina >= 10
      && !isDashing
    ){
      animator.SetTrigger("dashEvent");
      status.DrainStamina(10);
      isDashing = true;
      Invoke("ClearDash", 0.6f);
      return;  
    }

    if (direction.magnitude < 0.1f) return;
    animator.SetBool("isWalking", true);

    float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cameraLock.cam.eulerAngles.y;
    float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
    transform.rotation = Quaternion.Euler(0f, angle, 0f);
    Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;

    if (Input.GetKey(KeyCode.LeftShift) && acctualState.stamina >= 10) {
      animator.SetBool("isSprinting", true);
      acctualSpeed = acctualSpeed * 1.5f;
      isSprinting = true;
    } 

    controller.Move(moveDir.normalized * acctualSpeed * Time.deltaTime);
  }

  private IEnumerator SprintChecker() {
    WaitForSeconds wait = new WaitForSeconds(0.1f);

    while (true) {
      yield return wait;
      if (isSprinting) {
        status.DrainStamina(1.5f);
      }
    }
  }

  private void ClearDash() => isDashing = false;

  private void HandleAtack() {
    CharacterStats acctualState = status.TakeSnapshot();
    if (
      !Input.GetMouseButtonDown(0)
      || acctualState.stamina < 10
      || weaponItem.isActive
      || !isGrounded
    ) return;

    status.DrainStamina(10);
    weaponItem.PerformAtack(0.5f);
    animator.SetTrigger("atackEvent");
  }
}
