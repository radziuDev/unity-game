using System.Collections;
using UnityEngine;

public class Alarm : MonoBehaviour {
  public float alertRadius = 10f;
  public float runRadius = 10f;
  [SerializeField]
  private LayerMask enemyMask;

  [HideInInspector]
  public bool triggered = false;

  private void Start() {
    StartCoroutine(AlarmRoutine());
  }

  private IEnumerator AlarmRoutine() {
    WaitForSeconds wait = new WaitForSeconds(0.2f);
    
    while (true) {
      yield return wait;
      AlarmCheck();
    }
  }

  private void AlarmCheck () {
    Collider[] enemies = Physics.OverlapSphere(transform.position, runRadius, enemyMask);
    foreach (Collider enemy in enemies) {
      if (!triggered && enemy.GetComponent<Patrol>().targetedAlarm) {
        RunAlarm();
        break;
      }
    }
  }

  private void RunAlarm() {
    Collider[] enemies = Physics.OverlapSphere(transform.position, alertRadius, enemyMask);
    foreach (Collider enemy in enemies) {
      enemy.GetComponent<FOV>().StartRageMode();
      enemy.GetComponent<Patrol>().InitResetAlarmTriger();
      enemy.GetComponent<Patrol>().targetedAlarm = null;
    }

    triggered = true;
    Invoke("ClearTriggeredMark", 25f);
  }
  
  private void ClearTriggeredMark() => triggered = false;
}

