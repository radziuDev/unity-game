using System.Collections;
using Cinemachine;
using UnityEngine;

public class CameraLock : MonoBehaviour {
  private Transform currentTarget;
  
	[SerializeField]
  private CinemachineFreeLook freeLookCamera;
  public Transform cam;
  [SerializeField]
  private GameObject camFollow;
  private FOV fov;

	[HideInInspector]
	public bool locked = false;
	private StatusBar targetStatusBar;
  
  private Vector3 lastKnownCameraPos;

	void Start() {	
    UnityEngine.Cursor.lockState = CursorLockMode.Locked;
    UnityEngine.Cursor.visible = false;

    freeLookCamera = GameObject.FindObjectOfType<CinemachineFreeLook>();
    fov = gameObject.GetComponent<FOV>();

		ClearLock();
	}

	void Update() {
    if (freeLookCamera.LookAt) lastKnownCameraPos = freeLookCamera.LookAt.position;

    if (locked && fov.target) {
      if (Input.GetAxis("Mouse ScrollWheel") != 0) fov.currentTarget += Input.GetAxis("Mouse ScrollWheel") > 0 ? 1 : -1;
    
      CancelInvoke("ClearLock");
      if (!fov.canSeeTarget) ClearLock();
      if (currentTarget != fov.target) {
        currentTarget = fov.target;
        if (targetStatusBar) {
          targetStatusBar.setTarget(false);
          targetStatusBar = null;
        }
        LockOnTarget();
      }

      Vector3 directionToTarget = (fov.target.position - transform.position).normalized;
      Quaternion lookRotation = Quaternion.LookRotation(new Vector3(directionToTarget.x, 0, directionToTarget.z));
      transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 100f);  
    } else if (!fov.target && locked) {
      Invoke("ClearLock", 0.2f);
    }
    
    if (!Input.GetMouseButtonDown(2)) return;

    if (!locked && fov.canSeeTarget) {
      LockOnTarget();
      currentTarget = fov.target;
    } else ClearLock();
	}

  private void LockOnTarget() {
    if (!fov.target) return;
    
    targetStatusBar = fov.target.GetComponent<StatusBar>();
    targetStatusBar.setTarget(true);

    StartCoroutine(SwitchTargetSmoothly(camFollow.transform));
    freeLookCamera.m_BindingMode = CinemachineTransposer.BindingMode.LockToTarget;
    freeLookCamera.m_RecenterToTargetHeading.m_enabled = true;
    locked = true;
    fov.agressiveMode = true;
  }

  private void ClearLock() {
    StartCoroutine(SwitchTargetSmoothly(gameObject.transform));
    freeLookCamera.m_BindingMode = CinemachineTransposer.BindingMode.SimpleFollowWithWorldUp;
    freeLookCamera.m_RecenterToTargetHeading.m_enabled = false;
    locked = false;
    fov.agressiveMode = false;

    if (!targetStatusBar) return;
    targetStatusBar.setTarget(false);
    targetStatusBar = null;
  }

  private IEnumerator SwitchTargetSmoothly(Transform target) {
    Vector3 targetPosition = freeLookCamera.LookAt ? freeLookCamera.LookAt.position : lastKnownCameraPos;
    float transitionDuration = 0.5f;
    float elapsedTime = 0f;
    GameObject tempObject = new GameObject();

    while (elapsedTime < transitionDuration) {
      tempObject.transform.position = Vector3.Lerp(targetPosition, target.position, elapsedTime / transitionDuration);   
      freeLookCamera.LookAt = tempObject.transform; 

      elapsedTime += Time.deltaTime;
      yield return null;
    }

    GameObject.Destroy(tempObject);
    freeLookCamera.LookAt = target;
  }
}
