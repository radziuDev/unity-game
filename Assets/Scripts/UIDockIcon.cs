using System.Collections;
using TMPro;
using UnityEngine;

public class UIDockIcon : MonoBehaviour {
  [SerializeField]
  private GameObject cover;
  [SerializeField]
  private GameObject errorCover;
  [SerializeField]
  private GameObject cooldown;
  private TextMeshProUGUI text;

  private bool onCooldown = false;

  private void Start() {
    text = cooldown.GetComponent<TextMeshProUGUI>();
  }

  public void SetCooldown (float transitionDuration) => StartCoroutine(SetOnCooldown(transitionDuration));
  private IEnumerator SetOnCooldown(float transitionDuration) {
    float elapsedTime = 0f;
    cooldown.SetActive(true);
    cover.SetActive(true);
    onCooldown = true;

    while (elapsedTime < transitionDuration) {
      elapsedTime += Time.deltaTime;
      float remainingTime = transitionDuration - elapsedTime;
      text.text = $"{remainingTime.ToString("F1")}";
      yield return null;
    }

    onCooldown = false;
    cooldown.SetActive(false);
    cover.SetActive(false);
  }

  public void SetError() {
    if (onCooldown) return;
    StartCoroutine(SetOnError());
  }
  
  private IEnumerator SetOnError() {
    errorCover.SetActive(true);
    float transitionDuration = 0.5f;
    float elapsedTime = 0f;

    while (elapsedTime < transitionDuration) {
      elapsedTime += Time.deltaTime;
      yield return null;
    }

    errorCover.SetActive(false);
  }

  public void SetOn() {
    cover.SetActive(true);
    cooldown.SetActive(true);
    text.text = "ON";
  }

  public void SetOff() {
    cover.SetActive(false);
    cooldown.SetActive(false);
  }
}
