using System.Collections;
using UnityEngine;

public class Weapon : MonoBehaviour {
  private CapsuleCollider capsuleCollider;
    
  [SerializeField]
  private bool isEnemyWeapon = false;
  [SerializeField]
  private int damage = 10;

  private enum WeaponType {
    Melee,
    Range,
    Ammo,
  };

  [SerializeField]
  private WeaponType weaponType;
  [SerializeField]
  private GameObject ammoPrefab;
  [SerializeField]
  private GameObject spawner;
    
  [HideInInspector]
  public bool isActive = false;

  private void Start() {
    capsuleCollider = gameObject.GetComponent<CapsuleCollider>();
  }
  
  private void OnTriggerStay(Collider collision) {
    if (
      (collision.gameObject.layer == LayerMask.NameToLayer("Wall") && weaponType == WeaponType.Ammo)
      || (collision.gameObject.layer == LayerMask.NameToLayer("Ground") && weaponType == WeaponType.Ammo)
    ) {
      Object.Destroy(gameObject);
      return;
    }

    if (!collision.CompareTag(isEnemyWeapon ? "Player" : "Enemy") || !isActive) return;

    StatusBar statusBar = collision.gameObject.GetComponent<StatusBar>();
    if (!statusBar) return;

    isActive = false;
    StartCoroutine(DealDmg(statusBar));
  }

  private IEnumerator DealDmg(StatusBar statusBar) {
    yield return new WaitForSeconds(0.2f);
    if (statusBar != null) statusBar.TakeDamage(damage);
  }
  
  public void PerformAtack(float clearDealy) {
    isActive = true;
    Invoke("ClearAtack", clearDealy);

    if (weaponType == WeaponType.Range) Invoke("SpawnAmmo", 0.8f);
  }

  private void SpawnAmmo() {
    GameObject player = GameObject.FindGameObjectWithTag("Player");
    if (!player) return;

    Vector3 direction = player.transform.position - transform.position;
    direction.y = direction.y + 1f;
    direction.Normalize();

    GameObject projectile = Instantiate(ammoPrefab, spawner.transform.position, Quaternion.identity);
    projectile.SetActive(true);
    
    projectile.transform.rotation = Quaternion.LookRotation(direction);
    projectile.transform.Rotate(0f, 180f, 0f); // TODO: Rotate prefab

    projectile.GetComponent<Weapon>().isActive = true;
    Rigidbody projectileRigidbody  = projectile.GetComponent<Rigidbody>();
    projectileRigidbody.velocity = direction * 50f;

    Object.Destroy(projectile, 8f);
  }

  public void ClearAtack() {
    isActive = false;
  }
}