using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FOV : MonoBehaviour {
  private Patrol patrolSctipt;
  
  public bool isEnemy = true;
  private GameObject targetReference;

  public float radius;
  [Range(0, 360)]
  public float angle;
  public LayerMask targetMask;
  public LayerMask allayMask;
  public float alertRadius = 15;
  public LayerMask obstructionMask;

  [HideInInspector]
  public Transform target;
  [HideInInspector]
  private Collider oldTarget;
  [HideInInspector]
  public int currentTarget = 0;
  private int oldCurrent = 0;
  [HideInInspector]
  public bool canSeeTarget = true;
  [HideInInspector]
  public bool agressiveMode = false;
  private float rampageTime = 25f;

  Camera playerCamera;

  private void Start() {
    if (isEnemy) patrolSctipt = gameObject.GetComponent<Patrol>();
      else playerCamera = Camera.main;
    StartCoroutine(FOVRutine());
  }

  private IEnumerator FOVRutine() {
    WaitForSeconds wait = new WaitForSeconds(0.2f);
    
    while (true) {
      yield return wait;
      FieldOfViewCheck();
      SpreadRage();
    }
  }

  private void FieldOfViewCheck() {
    Collider[] rangeChecks = Physics.OverlapSphere(transform.position, radius, targetMask);
    if (rangeChecks.Length != 0) {
      List<Collider> avalibleChecks = new List<Collider>();

      for (int i = 0; i < rangeChecks.Length; i++) {
        Transform tempTarget = rangeChecks[i].transform;
        Vector3 tempDirectionToTarget = (tempTarget.position - transform.position).normalized;

        Vector3 forward = isEnemy ? transform.forward : Camera.main.transform.forward;
        if (Vector3.Angle(forward, tempDirectionToTarget) < (agressiveMode ? 360 : angle) / 2) {
          float distanceToTarget = Vector3.Distance(transform.position, tempTarget.position);        
          canSeeTarget = agressiveMode 
            ? distanceToTarget < radius
            : !Physics.Raycast(transform.position, tempDirectionToTarget, distanceToTarget, obstructionMask);
            
          if (canSeeTarget) avalibleChecks.Add(rangeChecks[i]);
        }
      }

      if (avalibleChecks.Count > 0) {
        int oldColiderPosition = FindColliderIndex(oldTarget, avalibleChecks);

        int index = oldColiderPosition >= 0 && currentTarget == oldCurrent
          ? oldColiderPosition
          : (currentTarget % avalibleChecks.Count + avalibleChecks.Count) % avalibleChecks.Count;

        target = avalibleChecks[index].transform;
        oldTarget = avalibleChecks[index];
        oldCurrent = currentTarget;
        canSeeTarget = true;

        if (!agressiveMode) StartRageMode();
      } else canSeeTarget = false;
    } else if (canSeeTarget) canSeeTarget = false;

    if (agressiveMode && !canSeeTarget) Invoke("ClearRageMode", rampageTime);
      else CancelInvoke("ClearRageMode");
  }

  private void SpreadRage() {
    if (!agressiveMode && isEnemy) return;
    Collider[] enemies = Physics.OverlapSphere(transform.position, alertRadius, allayMask);
    foreach (Collider enemy in enemies) {
      enemy.GetComponent<FOV>().StartRageMode();
    }
  }

  private int FindColliderIndex(Collider target, List<Collider> colliderList) {
    for (int i = 0; i < colliderList.Count; i++)
      if (colliderList[i] == target) return i;
    
    return -1;
  }

  public void StartRageMode() {
    if (!isEnemy) return;
    patrolSctipt.isPatroling = false;
    agressiveMode = true;
  }

  private void ClearRageMode() {
    if (!isEnemy) return;
    patrolSctipt.isPatroling = true;
    agressiveMode = false;
  }
}
