using UnityEngine;

public class Patrol : MonoBehaviour {
  private FOV fov;
  private Enemy enemy;

  [HideInInspector]
  public Collider targetedAlarm;
  public bool triggerAlarm = false;

  public float distance = 10f;
  private Vector3 startPoint;
  private Vector3 endPoint;
  [SerializeField]
  private float patrolSpeed = 4f;
  [SerializeField]
  private float iddleTime = 3f;

  [HideInInspector]
  public bool isPatroling = true;
  private bool isRetracting = false;
  private bool isIdle = false;

  private void Start() {
    fov = gameObject.GetComponent<FOV>();
    enemy = gameObject.GetComponent<Enemy>();
    startPoint = transform.position;
    endPoint = transform.position + transform.forward * distance;
  }

  private void Update() {
    if (triggerAlarm) HandleAlarm();
    if (!isPatroling) return;

    Vector3 destination = isRetracting ? endPoint : startPoint;
    Vector3 direction = destination - transform.position;

    Vector3 directionX = 
      new Vector3(destination.x, 0, destination.z)
      - new Vector3(transform.position.x, 0, transform.position.z);
    
    if (directionX.magnitude <= 1f) {
      isRetracting = !isRetracting;
      destination = isRetracting ? endPoint : startPoint;
      
      isIdle = true;
      Invoke("ClearIdle", iddleTime);
    }

    if (isIdle) return;
    enemy.HandleMovement(destination, patrolSpeed);
  }

  private void ClearIdle () => isIdle = false;

  private void HandleAlarm() {
    if (targetedAlarm && fov.agressiveMode) {
      if (targetedAlarm.gameObject.GetComponent<Alarm>().triggered) {
        targetedAlarm = null;
        triggerAlarm = true;
      } else enemy.HandleMovement(targetedAlarm.transform.position);
      return;
    }

    if (targetedAlarm) {
      targetedAlarm = null;
      triggerAlarm = true;
      return;
    }

    if (fov.agressiveMode) {
      LayerMask alarmLayerMask = LayerMask.GetMask("Alarm");
      Collider[] alarms = Physics.OverlapSphere(transform.position, fov.radius, alarmLayerMask);
      if (alarms.Length <= 0) return; 
      if (!alarms[0].gameObject.GetComponent<Alarm>().triggered) targetedAlarm = alarms[0];
    }
  }

  public void InitResetAlarmTriger() {
    triggerAlarm = false;
    CancelInvoke("ResetAlarmTriger");
    Invoke("ResetAlarmTriger", 26f);
  }

  private void ResetAlarmTriger () {
    if (fov.agressiveMode) {
      Invoke("ResetAlarmTriger", 2f);
    } else triggerAlarm = true;
  }
}
