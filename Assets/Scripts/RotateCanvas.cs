using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCanvas : MonoBehaviour {
  void Update() {
    transform.LookAt(transform.position + Camera.main.transform.forward, Vector3.up);
  }
}
