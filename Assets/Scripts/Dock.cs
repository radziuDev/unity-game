using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Dock : MonoBehaviour { 
  [SerializeField]
  private GameObject player;
  private StatusBar status;
  private ThirdPersonMovement playerMov;
  
  [SerializeField]
  private UIDockIcon atackUIIcon;
  [SerializeField]
  private UIDockIcon dashingUIIcon;
  [SerializeField]
  private UIDockIcon dagerUIIcon;
  [SerializeField]
  private UIDockIcon potionUIIcon;
  [SerializeField]
  private UIDockIcon cloakUIIcon;
  
  [SerializeField]
  private GameObject dager;
  [SerializeField]
  private GameObject cloak;
  [SerializeField]
  private GameObject shadowVision;

  private bool canUseDager = true;
  private bool canUsePotion = true;
  private bool isCloakeOn = false;


  public TextMeshProUGUI treasureCounterUi;
  [HideInInspector]
  public int treasureCounter = 0;

  void Start() {
    status = player.GetComponent<StatusBar>();      
    playerMov = player.GetComponent<ThirdPersonMovement>();      
    StartCoroutine(CloakChecker());
  }

  void Update() {
    HandleAtack();
    HandleDash();
    HandleDager();
    HandleHeal();
    HandleCloak();
    HandleCounter();
  }

  private void HandleCounter() {
    treasureCounterUi.text = $"Treasure picked: {treasureCounter}";
  }

  private void HandleAtack() {
    if (!Input.GetMouseButtonDown(0)) return;
    CharacterStats acctualState = status.TakeSnapshot();
  
    if (
      acctualState.stamina < 10  
      || playerMov.weaponItem.isActive
      || !playerMov.isGrounded
    ) {
      atackUIIcon.SetError();
      return;
    } 

    atackUIIcon.SetCooldown(0.5f);
  }

  private void HandleDash() {
    if (!Input.GetKey(KeyCode.LeftControl)) return;
    CharacterStats acctualState = status.TakeSnapshot();

    if (acctualState.stamina >= 10 && !playerMov.isDashing) dashingUIIcon.SetCooldown(0.6f);
      else dashingUIIcon.SetError(); 
  }

  private void HandleHeal() {
    if (!Input.GetKeyDown(KeyCode.Alpha2)) return;

    if (canUsePotion) {
      status.Heal(20f);
      potionUIIcon.SetCooldown(20f);
      canUsePotion = false;
      Invoke("ClearPotion", 20f);
    } else potionUIIcon.SetError();
  }

  private void HandleDager() {
    if (!Input.GetKeyDown(KeyCode.Alpha1)) return;
    CharacterStats acctualState = status.TakeSnapshot();

    if (canUseDager && acctualState.mana > 20) {
      status.DrainMana(20f);
      dagerUIIcon.SetCooldown(5f);
      canUseDager = false;
      Invoke("ClearDager", 5f);

      Vector3 spawnPos = player.transform.position + Vector3.up * 2f;
      GameObject projectile = Instantiate(dager, spawnPos, Quaternion.identity);
      projectile.GetComponent<Weapon>().isActive = true;

      Rigidbody projectileRigidbody = projectile.GetComponent<Rigidbody>();
      projectile.transform.rotation = player.transform.rotation;
      projectileRigidbody.velocity = projectile.transform.forward * 40f;
    } else dagerUIIcon.SetError();
  }

  private void HandleCloak() {
    CharacterStats acctualState = status.TakeSnapshot();
    if (acctualState.mana < 2 && isCloakeOn) {
      cloak.SetActive(false);
      shadowVision.SetActive(false);
      isCloakeOn = false;
      cloakUIIcon.SetOff();
    }

    if (!Input.GetKeyDown(KeyCode.Alpha3)) return;
    if (isCloakeOn) {
      cloak.SetActive(false);
      shadowVision.SetActive(false);
      isCloakeOn = false;
      cloakUIIcon.SetOff();
      return;
    } 

    cloak.SetActive(true);
    shadowVision.SetActive(true);
    isCloakeOn = true;
    cloakUIIcon.SetOn();
  }

  private IEnumerator CloakChecker() {
    WaitForSeconds wait = new WaitForSeconds(0.1f);

    while (true) {
      yield return wait;
      if (isCloakeOn) {
        status.DrainMana(1.5f);
      }
    }
  }

  private void ClearPotion() => canUsePotion = true;
  private void ClearDager() => canUseDager = true;
}
